# Secure Software Development 101 Source Code 

A basic and simple XSS example for Business Services Software Developers to use in Secure Software Development 101 short course. The aim of the code is to be scanned with a SAST tool and for developers to remedy the vulnerability and validate that the fix has been applied via a second scan.

## Getting started

- Clone this repository to your local machine using git 
- Use your IDE to open the solution
    - If possible start a development server to serve index.html
    - Ensure your API key for CheckMarx is created and configured in the CX CLI tool 
    - Ensure your API key is configured for the IDE plugin

### Example
```sh
foo@bar:~$ git clone https://gitlab.unimelb.edu.au/ddejager/xss-ssdv-101.git
Cloning into 'xss-ssdv-101'...
remote: Enumerating objects: 8, done.
remote: Counting objects: 100% (8/8), done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 8 (delta 1), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (8/8), done.
Resolving deltas: 100% (1/1), done.
```

 
## Support
- Reach out to the Cyber Security Team via it-security-team@lists.unimelb.edu.au for support 